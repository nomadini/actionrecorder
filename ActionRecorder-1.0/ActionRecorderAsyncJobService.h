#ifndef ActionRecorderAsyncJobService_H
#define ActionRecorderAsyncJobService_H



#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
class EntityToModuleStateStats;
#include "EntityToModuleStateStatsPersistenceService.h"
#include "MySqlPixelService.h"
#include "DateTimeMacro.h"
#include <tbb/concurrent_hash_map.h>
#include "MySqlSegmentService.h"
#include "DataReloadService.h"
#include <boost/thread.hpp>
class AdInteractionRecordingModule;
class ActionRecorderAsyncJobService {

public:

EntityToModuleStateStats* entityToModuleStateStats;
TimeType lastTimeDataWasReloadedInSeconds;
EntityToModuleStateStatsPersistenceService* entityToModuleStateStatsPersistenceService;
bool actionRecorderIsUpAndRunning;
MySqlPixelService* mySqlPixelService;
AdInteractionRecordingModule* adInteractionRecordingModule;
MySqlSegmentService* mySqlSegmentService;
std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<Pixel> > > keyToPixelMap;
DataReloadService* dataReloadService;
ActionRecorderAsyncJobService(EntityToModuleStateStats* entityToModuleStateStats);

std::shared_ptr<gicapods::AtomicBoolean> readyToProcessRequests;

void refreshData();

virtual ~ActionRecorderAsyncJobService();
void startAsyncThreads();

void runEveryMinute();
};



#endif
