
#include "ActionRecorderFunctionalTestsHelper.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>


std::shared_ptr<ActionRecorder> ActionRecorderFunctionalTestsHelper::actionRecorder;
int ActionRecorderFunctionalTestsHelper::argc;
char** ActionRecorderFunctionalTestsHelper::argv;

void ActionRecorderFunctionalTestsHelper::init(int argc, char *argv[]) {
        ActionRecorderFunctionalTestsHelper::argc = argc;
        ActionRecorderFunctionalTestsHelper::argv = argv;
}
