

#include "SaveActionPixelsFromRequest.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "HttpUtil.h"
#include "TestsCommon.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "PixelDeviceHistoryCassandraService.h"

#include "ApplicationContext.h"
#include "CollectionUtil.h"
#include "GUtil.h"
#include "ConfigService.h"
#include "Device.h"
#include <boost/foreach.hpp>
#include "BeanFactory.h"
#include "AsyncThreadPoolService.h"

void SaveActionPixelsFromRequest::SetUp( ) {
        // code here will execute just before the test ensues

        beanFactory->mySqlOfferService()->deleteAllOffersAndAssociatedPixelsAndModels;
        beanFactory->mySqlModelService()->deleteAll;
        /** truncate all the device histories */
        beanFactory->deviceFeatureHistoryCassandraService()->deleteAll;
        beanFactory->featureDeviceHistoryCassandraService()->deleteAll;
}

void SaveActionPixelsFromRequest::TearDown( ) {
        // code here will be called just after the test completes
        // ok to through exceptions from here if need be

        ApplicationContext::getThreadsInterruptedFlag()->setValue(true);
        MLOG(3)<<"going to sleep to  let the threads break out of their loops....";
        gicapods::Util::sleepViaBoost(_L_,5);


}


std::string SaveActionPixelsFromRequest::givenAPixelLoadRequest(std::string offerId, std::string pixelId) {

        std::string actionRecorderHostUrl = configService->get("actionRecorderHostUrl");
        std::string urlToActionTakers("__ACTION_RECORDER_URL__/pixel?offerId=__OFFER_ID__&pixelId=__PIXEL_ID__");
        urlToActionTakers = StringUtil::replaceString(urlToActionTakers, "__ACTION_RECORDER_URL__", actionRecorderHostUrl);
        urlToActionTakers = StringUtil::replaceString(urlToActionTakers, "__OFFER_ID__", offerId);
        urlToActionTakers = StringUtil::replaceString(urlToActionTakers, "__PIXEL_ID__", pixelId);

        LOG(INFO)<<"sending pixel request, url "<<urlToActionTakers;
        return urlToActionTakers;
}

void SaveActionPixelsFromRequest::whenRequestHitsTheActionRecorderService(std::string urlToActionTakers) {

}

void SaveActionPixelsFromRequest::thenDeviceHasAGicapodsId() {
//        std::string gicapodsId = HttpUtil::getCookieFromResponse(*actualResponse, "gicapodsId");

}

void SaveActionPixelsFromRequest::thenDeviceHasAHistoryContainingTheSourceOfRequest(std::string urlToActionTakers,
                                                                                    std::string sourceOfRequest) {

        Poco::Net::HTTPResponse actualResponse;

        HttpUtil::sendGetRequestAndGetFullResponse(urlToActionTakers, actualResponse);
        auto configService = std::make_shared<gicapods::ConfigService>(
                "actionrecorder.properties", "common-test.properties");
        auto entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();

        auto cassandraDriver = std::make_shared<CassandraDriver>(configService, entityToModuleStateStats);

        auto httpUtilService = std::make_shared<HttpUtilService>();

        auto deviceFeatureHistoryCassandraService =
                std::make_shared<DeviceFeatureHistoryCassandraService>(cassandraDriver,
                                                                       httpUtilService,
                                                                       entityToModuleStateStats);

        auto device = std::make_shared<Device>(HttpUtil::getCookieFromResponse(actualResponse, "gicapodsId"), "");

        std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory =
                deviceFeatureHistoryCassandraService->readFeatureHistoryOfDevice
                        (device,
                        300,
                        Feature::getAllFeatureTypes());


        EXPECT_THAT(deviceFeatureHistory.size(), testing::Gt(0));
        // bool value = CollectionUtil::valueExistInMap<std::string, std::string>(deviceFeatureHistory->timeFeatureMap, sourceOfRequest);
        // EXPECT_THAT(value, testing::Eq(true));


//       EXPECT_THAT(modelFromDb->modelType, testing::Eq(expected->modelType));
//       EXPECT_THAT(modelFromDb->algorithmToModelBasedOn, testing::Eq(expected->algorithmToModelBasedOn));
//       EXPECT_THAT(modelFromDb->segmentSeedName, testing::Eq(expected->segmentSeedName));
//       EXPECT_THAT(modelFromDb->featureScoreMap.size(), testing::Gt(0));


}

TEST_F(SaveActionPixelsFromRequest, saveActionPixel) {

        MLOG(3)<<"starting SaveActionPixelsFromRequest test";
        std::string offerId = "12";
        std::string pixelId = "12";
        std::string urlToActionTakers = SaveActionPixelsFromRequest::givenAPixelLoadRequest(offerId,pixelId);
        SaveActionPixelsFromRequest::whenRequestHitsTheActionRecorderService(urlToActionTakers);
        SaveActionPixelsFromRequest::thenDeviceHasAGicapodsId();
        SaveActionPixelsFromRequest::thenDeviceHasAHistoryContainingTheSourceOfRequest(urlToActionTakers, "localhost");

        MLOG(3)<<"end of SaveActionPixelsFromRequest test!";

}


TEST_F(SaveActionPixelsFromRequest, saveActionPixelForRequestWithCookie) {

        std::string offerId = "12";
        std::string pixelId = "12";
        MLOG(3)<<"starting saveActionPixelForRequestWithCookie test";
        std::string urlToActionTakers = SaveActionPixelsFromRequest::givenAPixelLoadRequest(offerId,pixelId);

        std::string cookieValue = "abc";
        std::string featureName = "localhost";
        Poco::Net::HTTPResponse actualResponse;

        HttpUtil::sendGetRequestAndGetFullResponse("gicapodsId", cookieValue, actualResponse, urlToActionTakers);

        auto device = std::make_shared<Device>(
                HttpUtil::getCookieFromResponse(actualResponse, "gicapodsId"), "");



        auto configService = std::make_shared<gicapods::ConfigService>(
                "actionrecorder-test.properties", "common-test.properties");
        auto entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();

        auto cassandraDriver = std::make_shared<CassandraDriver> (configService,
                                                                  entityToModuleStateStats);

        auto httpUtilService = std::make_shared<HttpUtilService>();

        auto asyncThreadPoolService = std::make_shared<AsyncThreadPoolService>();
        asyncThreadPoolService->entityToModuleStateStats = entityToModuleStateStats;
        asyncThreadPoolService->runTasksInSeperateThread();
        auto deviceFeatureHistoryCassandraService = std::make_shared<DeviceFeatureHistoryCassandraService>
                                                            (cassandraDriver,
                                                            httpUtilService,
                                                            entityToModuleStateStats,

                                                            asyncThreadPoolService);

        std::vector <std::shared_ptr<DeviceFeatureHistory> > deviceFeatureHistory =
                deviceFeatureHistoryCassandraService->readFeatureHistoryOfDevice(
                        device,
                        300,
                        Feature::getAllFeatureTypes());


        EXPECT_THAT(deviceFeatureHistory.size(), testing::Gt(0));
        bool value = CollectionUtil::valueExistInMap<std::string, std::string>(deviceFeatureHistory.at(0)->timeFeatureMap, featureName);
        EXPECT_THAT(value, testing::Eq(true));
        EXPECT_THAT(device->getDeviceId(), testing::Eq(cookieValue));

        auto featureDeviceHistory = beanFactory->featureDeviceHistoryCassandraService
                                    ->readDeviceHistoryOfFeature(featureName, (TimeType) 1000, 10);

        EXPECT_THAT(featureDeviceHistory->devicehistories.size(), testing::Eq(1));
        auto deviceHistory = featureDeviceHistory->devicehistories.at(0);
        EXPECT_THAT(deviceHistory->device->getDeviceId(), testing::Eq(device->getDeviceId()));
        EXPECT_THAT(deviceHistory->device->getDeviceType(), testing::Eq(device->getDeviceType()));

        auto pixServ = beanFactory->pixelDeviceHistoryCassandraService();
        std::shared_ptr<PixelDeviceHistory> pixelDevHistory = pixServ->readDeviceHistoryOfPixel(pixelId, 1000);

        EXPECT_THAT(pixelDevHistory->pixelUniqueKey, testing::Eq(pixelId));


        for(map_t::value_type &entry :
            *pixelDevHistory->devicesHittingPixel ) {
                EXPECT_THAT(entry.second->deviceId, testing::Eq(cookieValue));
                EXPECT_THAT(entry.second->deviceTypeStr, testing::Eq("DESKTOP"));
        }

        MLOG(3)<<"end of saveActionPixelForRequestWithCookie test!";

}
