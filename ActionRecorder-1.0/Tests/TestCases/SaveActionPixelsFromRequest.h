//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef SaveActionPixelsFromRequest_H
#define SaveActionPixelsFromRequest_H

#include "HttpUtil.h"
#include <string>
#include <memory>
#include "TestsCommon.h"
class SaveActionPixelsFromRequest : public ::testing::Test {

    public :



    void SetUp();
    void TearDown();


    std::string givenAPixelLoadRequest(std::string offerId, std::string pixelId);
    void whenRequestHitsTheActionRecorderService(std::string urlToActionTakers);
    void thenDeviceHasAGicapodsId();
    void thenDeviceHasAHistoryContainingTheSourceOfRequest(std::string url, std::string sourceOfRequest);
};
#endif //CreatingModelFromSubmittedRequestInDB_H
