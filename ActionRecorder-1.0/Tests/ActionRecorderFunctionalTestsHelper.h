//
// Created by Mahmoud Taabodi on 11/29/15.
//

#ifndef GICAPODS_ActionRecorderFUNCTIONALTESTSHELPER_H
#define GICAPODS_ActionRecorderFUNCTIONALTESTSHELPER_H



#include "ActionRecorder.h"

class ActionRecorderFunctionalTestsHelper {

public:
    static std::shared_ptr<ActionRecorder> actionRecorder;
    static int argc;
    static char** argv;

    static void init(int argc, char *argv[]);
};
#endif //GICAPODS_ActionRecorderFUNCTIONALTESTSHELPER_H
