/*
 * ActionRecorder.cpp
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#include "GUtil.h"


#include "CassandraDriverInterface.h"
#include "ApplicationContext.h"
#include "ConfigService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "ActionRecorder.h"
#include "GUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "ActionRecorderUtil.h"
#include "BeanFactory.h"
#include "ActionRecorderFunctionalTestsHelper.h"
#include "SignalHandler.h"
#include <thread>

int runGoogleTests(int argc, char** argv) {
        // The following line must be executed to initialize Google Mock
        // (and Google Test) before running the tests.
        ::testing::InitGoogleMock(&argc, argv);
        ::testing::FLAGS_gmock_verbose ="info";

        return RUN_ALL_TESTS();
}

void readProperties() {
}

void runActionRecorder(){


        std::shared_ptr<ActionRecorder> actionRecorder(new ActionRecorder());
        ActionRecorderFunctionalTestsHelper::actionRecorder = actionRecorder;
        actionRecorder->run(ActionRecorderFunctionalTestsHelper::argc, ActionRecorderFunctionalTestsHelper::argv);


}

void givenActionRecorderRunning() {

        std::thread everyMinuteThread(runActionRecorder);
        gicapods::Util::sleepViaBoost(_L_, 4);//wait for actionRecorder to start up!!
        everyMinuteThread.detach();
}
int main(int argc, char** argv) {

        ActionRecorderUtil::setupActionRecorder();

        MLOG(3)<<" argc :  "<<argc<<" , argv : "<< StringUtil::toStr(*argv);

        ActionRecorderFunctionalTestsHelper::init(argc,argv);

        MLOG(3)<<" ActionRecorderFunctionalTests::argc :  "<<ActionRecorderFunctionalTestsHelper::argc<<" , "
                "ActionRecorderFunctionalTestsHelper::argv : "<<StringUtil::toStr(*ActionRecorderFunctionalTestsHelper::argv);

        std::string appName = "ActionRecorderTests";
        TempUtil::configureLogging (appName, argv);
        std::string propertyFileName = "ActionRecorderTests.properties";
        auto beanFactory = std::make_unique<BeanFactory>("testVersion1.0");
        beanFactory->commonPropertyFileName = "common-test.properties";
        beanFactory->propertyFileName = propertyFileName;
        beanFactory->appName = appName;
        beanFactory->initializeModules();

        givenActionRecorderRunning();
//    while(!ActionRecorderFunctionalTestsHelper::actionRecorder->actionRecorderIsUpAndRunning) {
//        //wait until the action recorder is running
//    }
        runGoogleTests(argc, argv);
        cassandraDriver->closeSessionAndCluster();
}
