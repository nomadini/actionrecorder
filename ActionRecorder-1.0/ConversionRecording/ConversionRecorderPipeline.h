#ifndef ConversionRecorderPipeline_H
#define ConversionRecorderPipeline_H


#include "ActionRecorderModule.h"
#include "ActionRecorderContext.h"
#include "AtomicLong.h"
#include <memory>
#include <string>
class EntityToModuleStateStats;

class ConversionRecorderPipeline {

public:

std::vector<ActionRecorderModule*> modules;
EntityToModuleStateStats* entityToModuleStateStats;
ConversionRecorderPipeline();
void process(std::shared_ptr<ActionRecorderContext> context);
void setModules();
};



#endif
