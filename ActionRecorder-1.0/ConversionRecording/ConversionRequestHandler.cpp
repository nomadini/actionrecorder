

#include "SignalHandler.h"
#include "ApplicationContext.h"
#include "StringUtil.h"
#include "ConversionRequestHandler.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "Device.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "HttpUtil.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>

#include "ActionRecorderContext.h"
#include "Poco/Net/NameValueCollection.h"
#include "Poco/Net/HTTPCookie.h"
#include "Poco/URI.h"

ConversionRequestHandler::ConversionRequestHandler() {

}

void ConversionRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                             Poco::Net::HTTPServerResponse &response) {

        try {
                context->httpResponsePtr = &response;
                entityToModuleStateStats->addStateModuleForEntity(
                        "REQUEST_RECIEVED",
                        "ConversionRequestHandler",
                        "ALL");
                Poco::Timestamp now;


                bool abortTheProcess = configureContext(request);
                if (!abortTheProcess) {
                        context->httpRequestPtr = &request;
                        context->httpResponsePtr = &response;

                        conversionRecorderPipeline->process(context);

                }

                Poco::Timestamp::TimeDiff diff = now.elapsed(); // how long did it take?

                MLOG(3)<<"ActionRecorder Latency  "<<diff / 1000<<" milliseconds";

                //setEmptyResponse already sets the status to 204 but
                //i have include the line below to make sure, to setEmptyResponse
                //will not affect this code here

                //this is just to assert cookie has been written
                Poco::Net::NameValueCollection cookieMapOutgoing;
                cookieMapOutgoing.add("nomadiniDeviceId", context->device->getDeviceId());

                Poco::Net::HTTPCookie cookieOut(cookieMapOutgoing);
                response.addCookie(cookieOut);


                HttpUtil::getCookieFromResponse(response, "nomadiniDeviceId");

                entityToModuleStateStats->addStateModuleForEntity(
                        "SENDING_GOOD_RESPONSE",
                        "ConversionRequestHandler",
                        "ALL");
                HttpUtil::setEmptyResponse(response);
                return;
        } catch (std::exception const &e) {
                LOG(ERROR)<<"error happening when handling request  "<< boost::diagnostic_information (e);
                LOG(INFO)<<"sending no bad request as a result of exception";
                entityToModuleStateStats->addStateModuleForEntity(
                        "EXCEPTION_IN_HANDLER",
                        "ConversionRequestHandler",
                        "ALL");
                gicapods::Util::showStackTrace();
                HttpUtil::setBadRequestResponse (response, e.what());
        }

        catch (...) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "UNKNOW_EXCEPTION_IN_HANDLER",
                        "ConversionRequestHandler",
                        "ALL");
                gicapods::Util::showStackTrace();
                HttpUtil::setBadRequestResponse (response, "unknown error");
        }


}

bool ConversionRequestHandler::configureContext(Poco::Net::HTTPServerRequest &request) {
        bool abortTheProcess = false;
        auto nameCounterPair2 = context->queryParams.find("pixelId");
        if (nameCounterPair2 != context->queryParams.end()) {
                context->pixelUniqueKey = StringUtil::toLowerCase(*(&nameCounterPair2->second));
        } else {
                HttpUtil::printQueryParamsOfRequest(request);
                throwEx("pixelId is required for action");
        }

        tbb::concurrent_hash_map<std::string, std::shared_ptr<Pixel> >::accessor accessor;
        if(this->keyToPixelMap->find(accessor, context->pixelUniqueKey)) {
                context->pixel = accessor->second.get();
        } else {
                entityToModuleStateStats->
                addStateModuleForEntity("ABORTING_PIXEL_ID_PARAM_NOT_FOUND",
                                        "ConversionRequestHandler",
                                        "pix" + context->pixelUniqueKey);
                abortTheProcess = true;
        }
        Poco::URI uri(request.getURI());
        context->domain = uri.getHost();

        return abortTheProcess;

}
