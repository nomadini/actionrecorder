#ifndef ConversionRequestHandler_H
#define ConversionRequestHandler_H


#include <string>
#include <memory>
#include <vector>
#include <set>

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "ConversionRecorderPipeline.h"
#include "ActionRecorderContext.h"
class EntityToModuleStateStats;
#include <tbb/concurrent_hash_map.h>
class ConversionRequestHandler;


class ConversionRequestHandler : public Poco::Net::HTTPRequestHandler {

public:
ConversionRecorderPipeline* conversionRecorderPipeline;
EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<ActionRecorderContext> context;
std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<Pixel> > > keyToPixelMap;

ConversionRequestHandler();

bool configureContext(Poco::Net::HTTPServerRequest &request);

void handleRequest(Poco::Net::HTTPServerRequest &request,
                   Poco::Net::HTTPServerResponse &response);
};
#endif
