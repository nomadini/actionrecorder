
#include "AtomicLong.h"
#include "ConversionRecorderPipeline.h"
#include "DeviceHistoryUpdaterModule.h"
#include "FeatureDeviceHistoryUpdaterModule.h"
#include "PixelDeviceHistoryUpdaterModule.h"
#include "CassandraDriverInterface.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "PixelDeviceHistoryCassandraService.h"
#include "VisitFeatureHistoryOfDeviceUpdaterModuleWrapper.h"
#include "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper.h"


ConversionRecorderPipeline::ConversionRecorderPipeline() {

}

void ConversionRecorderPipeline::setModules() {

}

void ConversionRecorderPipeline::process(std::shared_ptr<ActionRecorderContext> context) {

        for (auto it : modules) {
                it->process(context);
                entityToModuleStateStats->addStateModuleForEntity("LastModule-" + (*it).getName (),
                                                                  "ConversionRecorderPipeline",
                                                                  "ALL");
        }

}
