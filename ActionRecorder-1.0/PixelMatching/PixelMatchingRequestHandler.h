#ifndef PixelMatchingRequestHandler_H
#define PixelMatchingRequestHandler_H


#include <string>
#include <memory>
#include <vector>
#include <set>

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "PixelMatchingPipeline.h"
#include "ActionRecorderContext.h"
class EntityToModuleStateStats;

class PixelMatchingRequestHandler;


class PixelMatchingRequestHandler : public Poco::Net::HTTPRequestHandler {

public:
PixelMatchingPipeline* pixelMatchingPipeline;
EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<ActionRecorderContext> context;

PixelMatchingRequestHandler();
void configureContext(Poco::Net::HTTPServerRequest &request);
void handleRequest(Poco::Net::HTTPServerRequest &request,
                   Poco::Net::HTTPServerResponse &response);

};
#endif
