

#include "SignalHandler.h"
#include "ApplicationContext.h"
#include "StringUtil.h"
#include "PixelMatchingRequestHandler.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "Device.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "HttpUtil.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>

#include "ActionRecorderContext.h"
#include "Poco/Net/NameValueCollection.h"
#include "Poco/Net/HTTPCookie.h"
#include "Poco/URI.h"

PixelMatchingRequestHandler::PixelMatchingRequestHandler() {

}

void PixelMatchingRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                                Poco::Net::HTTPServerResponse &response) {

        try {
                Poco::Timestamp now;
                context->httpResponsePtr = &response;
                configureContext(request);

                pixelMatchingPipeline->process(context);

                Poco::Timestamp::TimeDiff diff = now.elapsed();         // how long did it take?
                MLOG(3)<<"ActionRecorder Latency  "<<diff / 1000<<" milliseconds";

                Poco::Net::NameValueCollection cookieMapOutgoing;
                cookieMapOutgoing.add("nomadiniDeviceId", context->device->getDeviceId());

                Poco::Net::HTTPCookie cookieOut(cookieMapOutgoing);
                response.addCookie(cookieOut);

                //this is to assert cookie is written
                HttpUtil::getCookieFromResponse(response, "nomadiniDeviceId");
                HttpUtil::setEmptyResponse(response);
                return;
        } catch (std::exception const &e) {
                LOG(ERROR)<<"error happening when handling request  "<< boost::diagnostic_information (e);
                LOG(INFO)<<"sending no bad request as a result of exception";
                HttpUtil::setBadRequestResponse (response, e.what());
        }

        catch (...) {
                LOG(ERROR)<<"unknown error happening when handling request";
                LOG(INFO)<<"sending no bad request as a result of exception";
                // CounterService::getCounterService()->count (
                //         "numberOfExceptionsInHandlingRequest");
                HttpUtil::setBadRequestResponse (response, "unknown error");
        }


}

void PixelMatchingRequestHandler::configureContext(Poco::Net::HTTPServerRequest &request) {

}
