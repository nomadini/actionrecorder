#ifndef GicapodsIdToPartnerIdMapperModule_H
#define GicapodsIdToPartnerIdMapperModule_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "AtomicLong.h"
#include "ActionRecorderModule.h"
class GicapodsIdToExchangeIdsMapCassandraService;

class GicapodsIdToPartnerIdMapperModule;


class GicapodsIdToPartnerIdMapperModule : public ActionRecorderModule {

public:
static constexpr const char* nomadiniDeviceId = "nomadiniDeviceId";
GicapodsIdToExchangeIdsMapCassandraService* gicapodsIdToExchangeIdsMapCassandraService;
GicapodsIdToPartnerIdMapperModule();
virtual ~GicapodsIdToPartnerIdMapperModule();

std::string getName();

void upsertMapping(
        std::string nomadiniDeviceId,
        std::string partnerId,
        std::string partnerName);

virtual void process(std::shared_ptr<ActionRecorderContext> context);

};

#endif
