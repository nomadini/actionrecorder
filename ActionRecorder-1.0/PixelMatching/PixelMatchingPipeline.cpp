
#include "AtomicLong.h"
#include "PixelMatchingPipeline.h"
#include "DeviceHistoryUpdaterModule.h"
#include "FeatureDeviceHistoryUpdaterModule.h"
#include "PixelDeviceHistoryUpdaterModule.h"
#include "CassandraDriverInterface.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "PixelDeviceHistoryCassandraService.h"
#include "VisitFeatureHistoryOfDeviceUpdaterModuleWrapper.h"
#include "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper.h"


PixelMatchingPipeline::PixelMatchingPipeline() {

}


void PixelMatchingPipeline::process(std::shared_ptr<ActionRecorderContext> context) {

        for (auto it : modules) {
                it->process(context);
        }

}
