#ifndef PixelMatchingPipeline_H
#define PixelMatchingPipeline_H


#include "ActionRecorderModule.h"
#include "ActionRecorderContext.h"
#include "AtomicLong.h"
#include <memory>
#include <string>
class PixelMatchingPipeline {

public:

std::vector<ActionRecorderModule*> modules;

PixelMatchingPipeline();
void process(std::shared_ptr<ActionRecorderContext> context);
void setModules();
};



#endif
