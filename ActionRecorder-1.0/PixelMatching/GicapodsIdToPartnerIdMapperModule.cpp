#include "GicapodsIdToPartnerIdMapperModule.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "GicapodsIdToExchangeIdsMap.h"
#include "Device.h"
#include "GicapodsIdToExchangeIdsMapCassandraService.h"

GicapodsIdToPartnerIdMapperModule::GicapodsIdToPartnerIdMapperModule() {

}

GicapodsIdToPartnerIdMapperModule::~GicapodsIdToPartnerIdMapperModule() {

}

std::string GicapodsIdToPartnerIdMapperModule::getName() {
        return "GicapodsIdToPartnerIdMapperModule";
}

//How does cookie syncing work ????

//      us      google   rubicon  cookie
//      -        g1        -        -      Event :  google sends us g1 id
//      m1       g1        -        m1     Event : we write the id m1 as our id and write it in cassandra as m1-g1
//      m1        -        r1       m1     Event : rubicon sends r1 id for the same browser, we read the cookie m1
//      m1       g1        r1       m1     Event : we read the m1 from cassandra and find g1 too, and update the m1-g1 to m1-g1-r1

// g1 is sent to us, we read the nomadiniDeviceId cookie, if there is no cookie
//we don't know the browser, thats a new browser, we create a new random id for
//nomadiniDeviceId and write as a cookie and write the mapping between our cookie and google


//next time, an exchange redirects this browser to us, we read the cookie first,
//if cookie exists, we have a mapping for browser, and we write a mapping for the exchangeId
//and our nomadiniDeviceId and we write the
void GicapodsIdToPartnerIdMapperModule::process(std::shared_ptr<ActionRecorderContext> context) {
        std::string partnerId;
        if (StringUtil::equalsIgnoreCase(context->exchangeName, "google") == true) {
                std::string googleId;
                auto pairPtr = context->queryParams.find("google_gid");
                if( pairPtr != context->queryParams.end()) {
                        googleId = pairPtr->second;
                }
                assertAndThrow(!googleId.empty());
                partnerId = googleId;
        }

        assertAndThrow(!partnerId.empty());
        assertAndThrow(!context->exchangeName.empty());

        upsertMapping(context->device->getDeviceId(),
                      partnerId,
                      context->exchangeName);

}

void GicapodsIdToPartnerIdMapperModule::upsertMapping(
        std::string nomadiniDeviceId,
        std::string partnerId,
        std::string partnerName) {
        assertAndThrow(!partnerId.empty());
        assertAndThrow(!partnerName.empty());

        auto gicapodsIdToExchangeIdsMap = std::make_shared<GicapodsIdToExchangeIdsMap>();
        gicapodsIdToExchangeIdsMap->nomadiniDeviceId = nomadiniDeviceId;
        gicapodsIdToExchangeIdsMap->exchangeNameToExchangeId.insert(
                std::make_pair(
                        partnerName, partnerId
                        ));
        gicapodsIdToExchangeIdsMapCassandraService->pushToWriteBatchQueue(gicapodsIdToExchangeIdsMap);
}
