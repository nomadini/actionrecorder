/*
 * ActionRecorderAsyncJobService.cpp
 *
 *  Created on: Nov 24, 2016
 *      Author: mtaabodi
 */

#include "GUtil.h"


#include "SignalHandler.h"
#include "ActionRecorderAsyncJobService.h"
#include "Pixel.h"
#include "ConfigService.h"
#include <boost/exception/all.hpp>
#include "PixelCacheService.h"
#include "ConverterUtil.h"
#include "DateTimeUtil.h"
#include "ApplicationContext.h"
#include <thread>

#include <boost/exception/all.hpp>
#include "ConfigService.h"
#include "BeanFactory.h"
#include "CacheService.h"
#include "AdInteractionRecordingModule.h"

ActionRecorderAsyncJobService::ActionRecorderAsyncJobService(EntityToModuleStateStats* entityToModuleStateStats) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        lastTimeDataWasReloadedInSeconds = DateTimeUtil::getNowInSecond();
}

ActionRecorderAsyncJobService::~ActionRecorderAsyncJobService() {

}

void ActionRecorderAsyncJobService::runEveryMinute() {
        while (!ApplicationContext::getThreadsInterruptedFlag()->getValue()) {

                try {
                        readyToProcessRequests->setValue(false);
                        //wait for current requests to be finished
                        gicapods::Util::sleepViaBoost(_L_, 3);
                        //this inserts the action taker segments that are in queue
                        mySqlSegmentService->insertSegmentsFromQueue();
                        adInteractionRecordingModule->recordEvents();

                        dataReloadService->reloadDataViaHttp();

                        refreshData();
                        gicapods::Util::printMemoryUsage();
                        readyToProcessRequests->setValue(true);
                        lastTimeDataWasReloadedInSeconds = DateTimeUtil::getNowInSecond();
                } catch(...) {
                        entityToModuleStateStats->addStateModuleForEntity(
                                "EXCEPTION_IN_REFRESHING_DATA",
                                "ActionRecorderAsyncJobService",
                                EntityToModuleStateStats::all,
                                EntityToModuleStateStats::exception
                                );
                }
                int oldnessOfReloadedData = DateTimeUtil::getNowInSecond() - lastTimeDataWasReloadedInSeconds;
                if (oldnessOfReloadedData > 300) {
                        entityToModuleStateStats->addStateModuleForEntity(
                                "DATA_IS_TOO_OLD",
                                "ActionRecorderAsyncJobService",
                                EntityToModuleStateStats::all,
                                EntityToModuleStateStats::exception
                                );
                }
                gicapods::Util::sleepViaBoost(_L_,60);
        }
}

void ActionRecorderAsyncJobService::refreshData() {
        auto allPixels = *dataReloadService->beanFactory->pixelCacheService->getAllEntities();
        tbb::concurrent_hash_map<std::string, std::shared_ptr<Pixel> >::accessor accessor;
        keyToPixelMap->clear();
        for(auto pixel : allPixels) {
                keyToPixelMap->insert(accessor, pixel->uniqueKey);
                accessor->second = pixel;
        }
}



void ActionRecorderAsyncJobService::startAsyncThreads() {

        MLOG(3)<<"going to run daemon threads for actionRecorder";
        refreshData();
        readyToProcessRequests->setValue(true);
        std::thread everyMinuteThread(&ActionRecorderAsyncJobService::runEveryMinute, this);
        everyMinuteThread.detach();

        this->entityToModuleStateStatsPersistenceService->startThread();

}
