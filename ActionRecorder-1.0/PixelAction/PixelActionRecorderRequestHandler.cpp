

#include "SignalHandler.h"
#include "ApplicationContext.h"
#include "StringUtil.h"
#include "PixelActionRecorderRequestHandler.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "Device.h"
#include "JsonArrayUtil.h"
#include "ConverterUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "HttpUtil.h"
#include "Offer.h"
#include "Pixel.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>

#include "ActionRecorderContext.h"
#include "Poco/Net/NameValueCollection.h"
#include "Poco/Net/HTTPCookie.h"
#include "Poco/URI.h"
#include "OfferPixelMapCacheService.h"
#include "OfferCacheService.h"

PixelActionRecorderRequestHandler::PixelActionRecorderRequestHandler() {

}

void PixelActionRecorderRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                                      Poco::Net::HTTPServerResponse &response) {

        try {
                context->httpResponsePtr = &response;
                entityToModuleStateStats->addStateModuleForEntity(
                        "PIXEL_MATCHING_REQUEST_RECIEVED",
                        "PixelActionRecorderRequestHandler",
                        "ALL");
                Poco::Timestamp now;


                bool abortTheProcess = configureContext(request);
                if (!abortTheProcess) {
                        context->httpRequestPtr = &request;
                        context->httpResponsePtr = &response;

                        pixelActionRecorderPipeline->process(context);

                }

                Poco::Timestamp::TimeDiff diff = now.elapsed(); // how long did it take?

                MLOG(3)<<"ActionRecorder Latency  "<<diff / 1000<<" milliseconds";

                //setEmptyResponse already sets the status to 204 but
                //i have include the line below to make sure, to setEmptyResponse
                //will not affect this code here


                entityToModuleStateStats->addStateModuleForEntity(
                        "SENDING_GOOD_RESPONSE",
                        "PixelActionRecorderRequestHandler",
                        "ALL");
                HttpUtil::setEmptyResponse(response);
                return;
        } catch (std::exception const &e) {
                LOG(ERROR)<<"error happening when handling request  "<< boost::diagnostic_information (e);
                LOG(INFO)<<"sending no bad request as a result of exception";
                entityToModuleStateStats->addStateModuleForEntity(
                        "EXCEPTION_IN_HANDLER",
                        "PixelActionRecorderRequestHandler",
                        "ALL");
                gicapods::Util::showStackTrace();
                HttpUtil::setBadRequestResponse (response, e.what());
        }

        catch (...) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "UNKNOW_EXCEPTION_IN_HANDLER",
                        "PixelActionRecorderRequestHandler",
                        "ALL");
                gicapods::Util::showStackTrace();
                HttpUtil::setBadRequestResponse (response, "unknown error");
        }


}

bool PixelActionRecorderRequestHandler::configureContext(Poco::Net::HTTPServerRequest &request) {
        bool abortTheProcess = false;

        getPixelIdParam(abortTheProcess, request);
        getDeviceGeoParams(abortTheProcess);
        Poco::URI uri(request.getURI());
        context->domain = uri.getHost();

        return abortTheProcess;

}

void PixelActionRecorderRequestHandler::getDeviceGeoParams(bool& abortTheProcess) {
        bool latIsSet = false;
        bool lonIsSet = false;
        auto nameCounterPair = context->queryParams.find("lat");
        if (nameCounterPair != context->queryParams.end()) {
                context->deviceLat = ConverterUtil::convertTo<double>(*(&nameCounterPair->second));
                latIsSet = true;
        }

        nameCounterPair = context->queryParams.find("lon");
        if (nameCounterPair != context->queryParams.end()) {
                context->deviceLon = ConverterUtil::convertTo<double>(*(&nameCounterPair->second));
                lonIsSet = true;
        }

        if(latIsSet || lonIsSet) {
                assertAndThrow(latIsSet && lonIsSet);
                context->deviceGeoIsAvailable = true;
        }
}

void PixelActionRecorderRequestHandler::getPixelIdParam(bool& abortTheProcess, Poco::Net::HTTPServerRequest &request) {

        auto nameCounterPair2 = context->queryParams.find("pixelId");
        if (nameCounterPair2 != context->queryParams.end()) {
                context->pixelUniqueKey = StringUtil::toLowerCase(*(&nameCounterPair2->second));
        } else {
                HttpUtil::printQueryParamsOfRequest(request);
                throwEx("pixelId is required for action");
        }

        tbb::concurrent_hash_map<std::string, std::shared_ptr<Pixel> >::accessor accessor;
        if(this->keyToPixelMap->find(accessor, context->pixelUniqueKey)) {
                context->pixel = accessor->second.get();
                auto offerIds = offerPixelMapCacheService->findOfferIdsMappedToPixelId(context->pixel->id);
                for (auto offerId : offerIds) {
                        auto offer = offerCacheService->findByEntityId(offerId);
                        context->offers.push_back(offer);
                }
        } else {
                entityToModuleStateStats->
                addStateModuleForEntity("ABORTING_PIXEL_ID_PARAM_NOT_FOUND",
                                        "PixelActionRecorderRequestHandler",
                                        "pix" + context->pixelUniqueKey);
                abortTheProcess = true;
        }

}
