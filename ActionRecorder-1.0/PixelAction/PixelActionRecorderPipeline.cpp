
#include "AtomicLong.h"
#include "PixelActionRecorderPipeline.h"
#include "DeviceHistoryUpdaterModule.h"
#include "NomadiniDeviceIdCookieReaderModule.h"
#include "FeatureDeviceHistoryUpdaterModule.h"
#include "PixelDeviceHistoryUpdaterModule.h"
#include "CassandraDriverInterface.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "PixelDeviceHistoryCassandraService.h"
#include "VisitFeatureHistoryOfDeviceUpdaterModuleWrapper.h"
#include "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper.h"


PixelActionRecorderPipeline::PixelActionRecorderPipeline() {

}

void PixelActionRecorderPipeline::process(std::shared_ptr<ActionRecorderContext> context) {

        for (auto it : modules) {
                it->process(context);
                entityToModuleStateStats->addStateModuleForEntity("LastModule-" + (*it).getName (), "PixelActionRecorderPipeline", "ALL");
        }

}
