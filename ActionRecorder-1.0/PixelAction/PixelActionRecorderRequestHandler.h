#ifndef PixelActionRecorderRequestHandler_H
#define PixelActionRecorderRequestHandler_H


#include <string>
#include <memory>
#include <vector>
#include <set>

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "PixelActionRecorderPipeline.h"
#include "ActionRecorderContext.h"
class OfferPixelMapCacheService;
class OfferCacheService;

class EntityToModuleStateStats;
#include <tbb/concurrent_hash_map.h>
class PixelActionRecorderRequestHandler;


class PixelActionRecorderRequestHandler : public Poco::Net::HTTPRequestHandler {

public:
OfferPixelMapCacheService* offerPixelMapCacheService;
OfferCacheService* offerCacheService;

PixelActionRecorderPipeline* pixelActionRecorderPipeline;
EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<ActionRecorderContext> context;
std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<Pixel> > > keyToPixelMap;

PixelActionRecorderRequestHandler();

bool configureContext(Poco::Net::HTTPServerRequest &request);
void getPixelIdParam(bool& abortTheProcess, Poco::Net::HTTPServerRequest &request);
void getDeviceGeoParams(bool& abortTheProcess);
void handleRequest(Poco::Net::HTTPServerRequest &request,
                   Poco::Net::HTTPServerResponse &response);
};
#endif
