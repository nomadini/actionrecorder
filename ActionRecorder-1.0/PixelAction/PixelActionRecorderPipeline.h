#ifndef PixelActionRecorderPipeline_H
#define PixelActionRecorderPipeline_H


#include "ActionRecorderModule.h"
#include "ActionRecorderContext.h"
#include "AtomicLong.h"
#include <memory>
#include <string>
class EntityToModuleStateStats;

class PixelActionRecorderPipeline {

public:

std::vector<ActionRecorderModule*> modules;
EntityToModuleStateStats* entityToModuleStateStats;
ActionRecorderModule* nomadiniDeviceIdCookieReaderModule;
ActionRecorderModule* deviceHistoryOfVisitFeatureUpdaterModuleWrapper;
ActionRecorderModule* featureDeviceHistoryUpdaterModuleWrapper;
ActionRecorderModule* pixelDeviceHistoryUpdaterModule;
ActionRecorderModule* pixelActionTakerSegmentWriterModule;
PixelActionRecorderPipeline();
void process(std::shared_ptr<ActionRecorderContext> context);
void setModules();
};



#endif
