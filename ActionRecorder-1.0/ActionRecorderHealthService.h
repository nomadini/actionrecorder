#ifndef ActionRecorderHealthService_H
#define ActionRecorderHealthService_H

namespace gicapods { class ConfigService; }
class EntityToModuleStateStats;
#include "AtomicBoolean.h"
#include "AtomicLong.h"
#include <tbb/concurrent_queue.h>
class ActionRecorderHealthService {


public:
EntityToModuleStateStats* entityToModuleStateStats;

ActionRecorderHealthService();

bool determineHealth();

virtual ~ActionRecorderHealthService();
};



#endif
