#ifndef ACTION_RECORDER_H
#define ACTION_RECORDER_H



#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServer.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "ActionRecorderRequestHandlerFactory.h"
#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
class EntityToModuleStateStats;
#include <tbb/concurrent_hash_map.h>

class ActionRecorder : public std::enable_shared_from_this<ActionRecorder>, public Poco::Util::ServerApplication {

public:

EntityToModuleStateStats* entityToModuleStateStats;

ActionRecorderRequestHandlerFactory* actionRecorderRequestHandlerFactory;

ActionRecorder(EntityToModuleStateStats* entityToModuleStateStats);

virtual ~ActionRecorder();

int main(const std::vector<std::string>& args);

};



#endif
