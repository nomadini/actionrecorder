/*
 * ActionRecorder.cpp
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#include "GUtil.h"


#include "SignalHandler.h"
#include "PixelActionRecorderRequestHandler.h"

#include "ActionRecorder.h"
#include "PocoHttpServer.h"
#include "ConfigService.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/ThreadPool.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Net/ServerSocket.h"

using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::HTTPServer;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPServerParams;
using Poco::Net::ServerSocket;

using Poco::Util::Option;
using Poco::Util::OptionSet;
#include <boost/exception/all.hpp>
#include "ConverterUtil.h"
#include "ApplicationContext.h"
#include <thread>

#include <boost/exception/all.hpp>
#include "ConfigService.h"

ActionRecorder::ActionRecorder(EntityToModuleStateStats* entityToModuleStateStats) {
        this->entityToModuleStateStats = entityToModuleStateStats;
}

ActionRecorder::~ActionRecorder() {

}

int ActionRecorder::main(const std::vector<std::string>& args) {

        MLOG(3)<<"going to run daemon threads for actionRecorder";

        auto configService = std::make_shared<gicapods::ConfigService>(
                "actionrecorder.properties", "common.properties");

        auto srv = PocoHttpServer::createHttpServer(configService.get(), actionRecorderRequestHandlerFactory);


        // start the HTTPServer
        MLOG(3)<<"ActionRecorder server about to start ";
        srv->start();
        // wait for CTRL-C or kill
        waitForTerminationRequest();
        // Stop the HTTPServer
        srv->stop();

        return Application::EXIT_OK;
}
