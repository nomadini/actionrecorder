


#include "AtomicBoolean.h"
#include "ApplicationContext.h"
#include "FileUtil.h"
#include "StringUtil.h"


std::shared_ptr<std::ofstream> ApplicationContext::getBidEventsAppender() {
        static std::shared_ptr<std::ofstream> srv = std::make_shared<std::ofstream>();
        return srv;
}

std::shared_ptr<gicapods::AtomicBoolean> ApplicationContext::getThreadsInterruptedFlag() {
        static std::shared_ptr<gicapods::AtomicBoolean> srv = std::make_shared<gicapods::AtomicBoolean>();
        return srv;
}

std::string ApplicationContext::getTransactionIdConstant(){
        static std::string srv = StringUtil::toStr("__TRN_ID__");
        return srv;
}

ApplicationContext::ApplicationContext() {

}

void ApplicationContext::info(const std::string& str) {

}
