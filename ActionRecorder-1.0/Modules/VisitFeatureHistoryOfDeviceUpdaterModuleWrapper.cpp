#include "VisitFeatureHistoryOfDeviceUpdaterModuleWrapper.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "DeviceFeatureHistory.h"
#include "Feature.h"
VisitFeatureHistoryOfDeviceUpdaterModuleWrapper::VisitFeatureHistoryOfDeviceUpdaterModuleWrapper() {
}

std::string VisitFeatureHistoryOfDeviceUpdaterModuleWrapper::getName() {
        return "VisitFeatureHistoryOfDeviceUpdaterModuleWrapper";
}

void VisitFeatureHistoryOfDeviceUpdaterModuleWrapper::process(std::shared_ptr<ActionRecorderContext> context) {


        if (context->device == nullptr || context->domain.empty()) {
                throwEx("unknown siteDomain or deviceType");
        }
        visitFeatureDeviceHistoryUpdaterModule->process(Feature::generalTopLevelDomain,
                                                        context->domain,
                                                        context->device);
}

VisitFeatureHistoryOfDeviceUpdaterModuleWrapper::~VisitFeatureHistoryOfDeviceUpdaterModuleWrapper() {

}
