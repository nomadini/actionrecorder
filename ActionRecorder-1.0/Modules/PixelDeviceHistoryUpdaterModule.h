#ifndef PixelDeviceHistoryUpdaterModule_H
#define PixelDeviceHistoryUpdaterModule_H


#include "ActionRecorderModule.h"
#include "ActionRecorderContext.h"
#include <memory>
#include <string>
class PixelDeviceHistoryCassandraService;
#include "ActionRecorderContext.h"


class PixelDeviceHistoryUpdaterModule : public ActionRecorderModule {

private:

public:

PixelDeviceHistoryCassandraService* pixelDeviceHistoryCassandraService;
PixelDeviceHistoryUpdaterModule(PixelDeviceHistoryCassandraService* pixelDeviceHistoryCassandraService);


std::string getName();

virtual void process(std::shared_ptr<ActionRecorderContext> context);
virtual ~PixelDeviceHistoryUpdaterModule();

};



#endif
