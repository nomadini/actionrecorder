#include "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper.h"
#include "DateTimeUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
DeviceHistoryOfVisitFeatureUpdaterModuleWrapper::DeviceHistoryOfVisitFeatureUpdaterModuleWrapper() {

}

std::string DeviceHistoryOfVisitFeatureUpdaterModuleWrapper::getName() {
        return "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper";
}



void DeviceHistoryOfVisitFeatureUpdaterModuleWrapper::process(std::shared_ptr<ActionRecorderContext> context) {
        if (context->device == nullptr || context->domain.empty()) {
                throwEx("unknown siteDomain or deviceType");
        }

        featureDeviceHistoryUpdaterModule->process(Feature::generalTopLevelDomain,
                                                   context->domain,
                                                   context->device);

}

DeviceHistoryOfVisitFeatureUpdaterModuleWrapper::~DeviceHistoryOfVisitFeatureUpdaterModuleWrapper() {

}
