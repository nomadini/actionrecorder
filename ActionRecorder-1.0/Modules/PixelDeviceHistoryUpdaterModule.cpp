#include "PixelDeviceHistoryUpdaterModule.h"
#include "DateTimeUtil.h"
#include "PixelDeviceHistory.h"
#include "GUtil.h"
#include "Device.h"
#include "PixelDeviceHistoryCassandraService.h"

PixelDeviceHistoryUpdaterModule::PixelDeviceHistoryUpdaterModule(
        PixelDeviceHistoryCassandraService* pixelDeviceHistoryCassandraService) {

        this->pixelDeviceHistoryCassandraService = pixelDeviceHistoryCassandraService;

}

std::string PixelDeviceHistoryUpdaterModule::getName() {
        return "PixelDeviceHistoryUpdaterModule";
}


void PixelDeviceHistoryUpdaterModule::process(std::shared_ptr<ActionRecorderContext> context) {

        entityToModuleStateStats->addStateModuleForEntity("PERSISTING_PIXEL_DEVICE_MAP",
                                                          "PixelDeviceHistoryUpdaterModule",
                                                          context->pixelUniqueKey);

        NULL_CHECK(context->device);
        auto pixelDeviceHistory = std::make_shared<PixelDeviceHistory>();
        pixelDeviceHistory->pixelUniqueKey = context->pixelUniqueKey;
        pixelDeviceHistory->
        devicesHittingPixel->
        insert(std::make_pair(
                       StringUtil::toStr(DateTimeUtil::getNowInMilliSecond()),
                       context->device));

        if (!pixelDeviceHistory->devicesHittingPixel->empty()) {
                pixelDeviceHistoryCassandraService->pushToWriteBatchQueue(pixelDeviceHistory);
        }
}

PixelDeviceHistoryUpdaterModule::~PixelDeviceHistoryUpdaterModule() {

}
