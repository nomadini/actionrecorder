#ifndef PixelActionTakerSegmentWriterModule_H
#define PixelActionTakerSegmentWriterModule_H


#include "ActionRecorderModule.h"
#include "ActionRecorderContext.h"
#include <memory>
#include <string>
class DeviceSegmentHistoryCassandraService;
#include "ActionRecorderContext.h"
#include "OfferSegmentCacheService.h"
class Advertiser;
class Pixel;
class Offer;
class Segment;
template <class T>
class CacheService;

class PixelActionTakerSegmentWriterModule : public ActionRecorderModule {

private:

public:
DeviceSegmentHistoryCassandraService* deviceSegmentHistoryCassandraService;
OfferSegmentCacheService* offerSegmentCacheService;

PixelActionTakerSegmentWriterModule();



std::string getName();

virtual void process(std::shared_ptr<ActionRecorderContext> context);
void recordActionTakerDeviceForEachOffer(
        std::shared_ptr<ActionRecorderContext> context,
        std::shared_ptr<Offer> offer);

void recordSegmentForDevice(
        std::shared_ptr<Segment> segment,
        std::shared_ptr<ActionRecorderContext> context);
virtual ~PixelActionTakerSegmentWriterModule();

};



#endif
