#ifndef ConversionDecider_H
#define ConversionDecider_H


#include "ActionRecorderModule.h"


template <class T>
class CassandraService;

class EventLog;

#include <memory>
#include <string>

class ActionRecorderContext;
class AdInteractionRecordingModule;
class TargetGroupOfferCacheService;
class Pixel;
class AdHistory;
class AdEntry;
class OfferPixelMapCacheService;
#include "RealTimeEntityCacheService.h"

class ConversionDecider : public ActionRecorderModule {

private:

public:
long dayInSeconds;
RealTimeEntityCacheService<AdHistory>* adHistoryCacheService;
TargetGroupOfferCacheService* targetGroupOfferCacheService;
OfferPixelMapCacheService* offerPixelMapCacheService;
AdInteractionRecordingModule* adInteractionRecordingModule;
CassandraService<EventLog>* eventLogCassandraService;

ConversionDecider();

std::string getName();
std::vector<int> getOffersAttachedToPixel(Pixel* pixel);
std::vector<int> findTargetGroupIdsForOfferIds(std::vector<int> offerIds);
void persistAConversion(std::shared_ptr<AdEntry> adEntry, std::shared_ptr<ActionRecorderContext> context);

virtual void process(std::shared_ptr<ActionRecorderContext> context);
virtual ~ConversionDecider();

};



#endif
