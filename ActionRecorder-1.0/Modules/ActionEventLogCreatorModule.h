#ifndef ActionEventLogCreatorModule_h
#define ActionEventLogCreatorModule_h


#include "Status.h"
#include <memory>
#include <string>
class OpportunityContext;
class EventLog;
#include "ActionRecorderModule.h"
class EventLogCassandraService;
#include <tbb/concurrent_queue.h>
#include <Poco/ThreadPool.h>
#include "CassandraServiceInterface.h"
#include "Object.h"

class ActionEventLogCreatorModule : public ActionRecorderModule, public Object {

public:
CassandraServiceInterface* eventLogCassandraService;
std::string appHostname;
std::string appVersion;
ActionEventLogCreatorModule(
								std::string appHostname,
								std::string appVersion);

virtual std::string getName();

virtual void process(std::shared_ptr<ActionRecorderContext> context);

std::shared_ptr<EventLog> createEventLogFrom(std::shared_ptr<ActionRecorderContext> context);
};


#endif
