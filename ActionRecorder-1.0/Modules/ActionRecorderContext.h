#ifndef ActionRecorderContext_H
#define ActionRecorderContext_H



#include "Status.h"
class StringUtil;
#include <unordered_map>
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
class EventLog;
class Pixel;
class Offer;
class Device;
class ActionRecorderContext;

class ActionRecorderContext {

private:

public:

const Poco::Net::HTTPServerRequest* httpRequestPtr;
Poco::Net::HTTPServerResponse* httpResponsePtr;
std::unordered_map<std::string, std::string> queryParams;
std::unordered_map<std::string, std::string> cookieMap;

std::shared_ptr<EventLog> actionEventLog;
std::shared_ptr<Device> device;
std::vector<std::shared_ptr<Offer> > offers;
double deviceLat;
double deviceLon;
bool deviceGeoIsAvailable;
int offerId;
std::string pixelUniqueKey;
Pixel* pixel;
std::string domain;
std::string exchangeName;
std::string siteCategory;
bool strictEventLogChecking;


std::string transactionId;
int partnerId;
std::string userTimeZone;
std::string deviceUserAgent;
std::string deviceIp;
std::string deviceCountry;
std::string deviceState;
std::string deviceCity;
std::string deviceZipcode;
double mgrs1km;
double mgrs100m;
double mgrs10m;

std::string appHostname;
std::string appVersion;

ActionRecorderContext();
std::string toString();

static std::shared_ptr<ActionRecorderContext> fromJson(std::string json);

std::string toJson();

virtual ~ActionRecorderContext();

};

#endif
