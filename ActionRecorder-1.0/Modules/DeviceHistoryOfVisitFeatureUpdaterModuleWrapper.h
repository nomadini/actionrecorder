#ifndef DeviceHistoryOfVisitFeatureUpdaterModuleWrapper_H
#define DeviceHistoryOfVisitFeatureUpdaterModuleWrapper_H


#include "ActionRecorderModule.h"
#include "ActionRecorderContext.h"
#include <memory>
#include <string>
#include "ActionRecorderContext.h"
#include "FeatureDeviceHistory.h"
#include "FeatureDeviceHistoryUpdaterModule.h"
//NOT USED IN ActionRecorder but we keep it for future use
class DeviceHistoryOfVisitFeatureUpdaterModuleWrapper : public ActionRecorderModule {

private:

public:
FeatureDeviceHistoryUpdaterModule* featureDeviceHistoryUpdaterModule;
DeviceHistoryOfVisitFeatureUpdaterModuleWrapper();

std::string getName();

std::string toString();

virtual void process(std::shared_ptr<ActionRecorderContext> context);
virtual ~DeviceHistoryOfVisitFeatureUpdaterModuleWrapper();

};



#endif
