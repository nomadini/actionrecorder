#ifndef ActionRecorderModule_H
#define ActionRecorderModule_H


#include "Status.h" //this is needed in every ActionRecorderModule
#include "ActionRecorderContext.h"
#include <memory>
#include <string>
#include "EntityToModuleStateStats.h"
#include "AtomicLong.h"
class EntityToModuleStateStats;

class ActionRecorderModule {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
ActionRecorderModule();

virtual void process(std::shared_ptr<ActionRecorderContext> context)=0;

virtual std::string getName()=0;

virtual ~ActionRecorderModule();

};



#endif
