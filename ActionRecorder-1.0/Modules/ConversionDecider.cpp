#include "ConversionDecider.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "Device.h"
#include "ActionRecorderContext.h"
#include "Pixel.h"
#include "CollectionUtil.h"
#include "IntegerVectorHolder.h"
#include "AdHistoryCassandraService.h"
#include "AdInteractionRecordingModule.h"
#include "OfferPixelMapCacheService.h"
#include "TargetGroupOfferCacheService.h"

ConversionDecider::ConversionDecider() {
        dayInSeconds = 86400;
}

std::string ConversionDecider::getName() {
        return "ConversionDecider";
}

std::vector<int> ConversionDecider::getOffersAttachedToPixel(Pixel* pixel) {

        return offerPixelMapCacheService->findOfferIdsMappedToPixelId(pixel->id);
}

std::vector<int>  ConversionDecider::findTargetGroupIdsForOfferIds(std::vector<int> offerIds) {
        std::vector<int> targetGroupIds;
        for (auto offerId : offerIds) {
                auto pair = targetGroupOfferCacheService->mapOfOfferIdToTargetGroupIds->getOptional(offerId);
                if (pair != nullptr) {
                        targetGroupIds = CollectionUtil::combineTwoLists(*pair->values, targetGroupIds);
                }
        }

        return targetGroupIds;
}

void ConversionDecider::process(std::shared_ptr<ActionRecorderContext> context) {

        entityToModuleStateStats->addStateModuleForEntity("PERSISTING_PIXEL_DEVICE_MAP",
                                                          "ConversionDecider",
                                                          context->pixelUniqueKey);
        //read adHistory of device...
        auto keyOfAdHistory = std::make_shared<AdHistory>(context->device);
        auto adHistory = adHistoryCacheService->readDataOptional(keyOfAdHistory);

        // find all the targetgroups and tgs that is assigned to this pixelUniqueKey

        std::vector<int> targetGroupIds  = findTargetGroupIdsForOfferIds(getOffersAttachedToPixel(context->pixel));
        if (targetGroupIds.empty()) {
                entityToModuleStateStats->addStateModuleForEntity("CONVERSION_PIXEL_HIT_FOR_NO_TARGETGROUPS",
                                                                  "ConversionDecider",
                                                                  EntityToModuleStateStats::all,
                                                                  EntityToModuleStateStats::exception);
        }
        for (auto targetGroupId : targetGroupIds) {
                auto adEntries = adHistory->getAdEntriesByTgId(targetGroupId);

                for(auto adEntry : adEntries) {
                        int recencyInSeconds = (DateTimeUtil::getNowInMilliSecond() - adEntry->timeAdShownInMillis) * 1000;
                        if (recencyInSeconds <= dayInSeconds * 7 ) {
                                persistAConversion(adEntry, context);
                                break;//break out of adEntries lookup for this targetGroup
                        } else {
                                entityToModuleStateStats->addStateModuleForEntity("CONVERSION_FOUND_BUT_WAS_TOO_OLD",
                                                                                  "ConversionDecider",
                                                                                  "tg" + _toStr(adEntry->targetGroupId));
                        }
                }
        }
}

void ConversionDecider::persistAConversion(std::shared_ptr<AdEntry> adEntry, std::shared_ptr<ActionRecorderContext> context) {
        auto conversion = std::make_shared<AdHistory>(context->device);

        entityToModuleStateStats->addStateModuleForEntity("ADDING_CONVERSION",
                                                          "ConversionDecider",
                                                          "tg" + _toStr(adEntry->targetGroupId),
                                                          EntityToModuleStateStats::important);
        adHistoryCacheService->
        realTimeEntityCassandraService->
        pushToWriteBatchQueue(conversion);

        auto impressionEventLogKey = std::make_shared<EventLog>(
                adEntry->transactionId,
                EventLog::EVENT_TYPE_IMPRESSION,
                EventLog::TARGETING_TYPE_IGNORE_FOR_NOW
                );
        auto impressionEventLog = eventLogCassandraService->readDataOptional(impressionEventLogKey);
        if (impressionEventLog == nullptr) {
                throwEx("cannot find impression for the conversion");
        }
        impressionEventLog->eventType = EventLog::EVENT_TYPE_CONVERSION;
        impressionEventLog->sourceEventId = adEntry->transactionId;
        impressionEventLog->eventId = EventLog::createRandomTransactionId();
        adInteractionRecordingModule->enqueueEventForPersistence(impressionEventLog);
}
ConversionDecider::~ConversionDecider() {

}
