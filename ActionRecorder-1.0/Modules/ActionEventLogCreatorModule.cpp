
#include "ActionEventLogCreatorModule.h"
#include "CollectionUtil.h"
#include "DateTimeUtil.h"
#include <tbb/concurrent_queue.h>
#include <thread>
#include "Encoder.h"
#include "ActionRecorderContext.h"
#include "EventLog.h"
#include "Device.h"
#include "Pixel.h"
#include "CollectionUtil.h"
#include "TargetGroup.h"
#include "JsonArrayUtil.h"
ActionEventLogCreatorModule::ActionEventLogCreatorModule(
								std::string appHostname,
								std::string appVersion)
								: Object(__FILE__) {
								this->appHostname = appHostname;
								this->appVersion = appVersion;
}

std::string ActionEventLogCreatorModule::getName() {
								return "ActionEventLogCreatorModule";
}

void ActionEventLogCreatorModule::process(std::shared_ptr<ActionRecorderContext> context) {
								//this module runs after we have selected a bid


								context->actionEventLog = createEventLogFrom(context);
								LOG_EVERY_N(INFO, 1000) << google::COUNTER<< "th sample eventLog : " << context->actionEventLog->toJson();

								entityToModuleStateStats->addStateModuleForEntity ("action_event_writing_to_queue",
																																																											"ActionEventLogCreatorModule",
																																																											"ALL");

								eventLogCassandraService->pushToWriteBatchQueue(
																std::static_pointer_cast<CassandraManagedType>(context->actionEventLog));

}

std::shared_ptr<EventLog> ActionEventLogCreatorModule::createEventLogFrom(
								std::shared_ptr<ActionRecorderContext> context) {

								context->transactionId = EventLog::createRandomTransactionId();

								auto eventLog = std::make_shared<EventLog>(
																context->transactionId,
																EventLog::EVENT_TYPE_ACTION,
																""
																);
								eventLog->eventTime = StringUtil::toStr(DateTimeUtil::getNowInMicroSecond());
								eventLog->partnerId = context->partnerId;
								eventLog->pixelId = context->pixel->id;
								assertAndThrow(eventLog->pixelId > 0);
								eventLog->userTimeZone = context->userTimeZone;
								eventLog->deviceUserAgent = context->deviceUserAgent;
								eventLog->deviceIp = context->deviceIp;
								eventLog->device = context->device;
								eventLog->deviceLat = context->deviceLat;
								eventLog->deviceLon = context->deviceLon;
								eventLog->deviceCountry = context->deviceCountry;
								eventLog->deviceState = context->deviceState;
								eventLog->deviceCity = context->deviceCity;
								eventLog->deviceZipcode = context->deviceZipcode;
								eventLog->deviceIpAddress = context->deviceIp;
								eventLog->mgrs1km  = context->mgrs1km;
								eventLog->mgrs100m  = context->mgrs100m;
								eventLog->mgrs10m  = context->mgrs10m;

								eventLog->appHostname = appHostname;
								eventLog->appVersion = appVersion;
								if (context->strictEventLogChecking) {
																//sometimes we want to record no_bid events in cassandra,
																//in BidEventRecorderModule so we set this boolean flag as false
																//and we don't check these

																assertAndThrow(!eventLog->eventTime.empty());
																assertAndThrow(!eventLog->eventType.empty());
																assertAndThrow(!eventLog->eventId.empty());
																assertAndThrow(!eventLog->deviceUserAgent.empty());
																assertAndThrow(!eventLog->deviceIp.empty());
																assertAndThrow(eventLog->deviceLat != 0);
																assertAndThrow(eventLog->deviceLon != 0);
																assertAndThrow(!eventLog->deviceCountry.empty());
																assertAndThrow(!eventLog->deviceCity.empty());
																assertAndThrow(!eventLog->deviceState.empty());
																assertAndThrow(!eventLog->deviceZipcode.empty());
																assertAndThrow(!eventLog->deviceIpAddress.empty());
																assertAndThrow(!eventLog->mgrs1km.empty());
																assertAndThrow(!eventLog->mgrs100m.empty());
																assertAndThrow(!eventLog->mgrs10m.empty());

																assertAndThrow(!eventLog->appHostname.empty());
																assertAndThrow(!eventLog->appVersion.empty());
								}
								return eventLog;
}
