#include "PixelActionTakerSegmentWriterModule.h"
#include "DateTimeUtil.h"
#include "PixelDeviceHistory.h"
#include "DeviceSegmentHistory.h"
#include "Pixel.h"
#include "Segment.h"
#include "DeviceSegmentPair.h"
#include "SegmentNameCreator.h"
#include "Device.h"
#include "DeviceSegmentHistoryCassandraService.h"
#include "CacheService.h"
#include "Pixel.h"
#include "Advertiser.h"
#include "Segment.h"
#include "OfferSegmentCacheService.h"
#include "Offer.h"
PixelActionTakerSegmentWriterModule::PixelActionTakerSegmentWriterModule() {

}

std::string PixelActionTakerSegmentWriterModule::getName() {
        return "PixelActionTakerSegmentWriterModule";
}


void PixelActionTakerSegmentWriterModule::process(std::shared_ptr<ActionRecorderContext> context) {

        for(auto offer : context->offers) {
                try {
                        recordActionTakerDeviceForEachOffer(context, offer);
                } catch (...) {
                        entityToModuleStateStats->addStateModuleForEntity("EXCEPTION_IN_RECORDING_DEVICE",
                                                                          "PixelActionTakerSegmentWriterModule",
                                                                          "pixel"+StringUtil::toStr(context->pixelUniqueKey));
                }
        }

}

void PixelActionTakerSegmentWriterModule::recordActionTakerDeviceForEachOffer(
        std::shared_ptr<ActionRecorderContext> context,
        std::shared_ptr<Offer> offer) {

        if (!StringUtil::equalsIgnoreCase(offer->audienceType,
                                          Offer::AUDIENCE_TYPE_ACTION_TAKER)) {
                return;
        }

        entityToModuleStateStats->addStateModuleForEntity("PERSISTING_OFFER_ACTION_TAKER_DEVICE",
                                                          "PixelActionTakerSegmentWriterModule",
                                                          "pixel"+StringUtil::toStr(context->pixelUniqueKey));


        auto pair = offerSegmentCacheService->offerIdToSegments->find(offer->id);
        if (pair == offerSegmentCacheService->offerIdToSegments->end()) {
                return;
        }
        auto segments = pair->second;

        for(auto segment : *segments) {
                recordSegmentForDevice(segment, context);
        }
}

void PixelActionTakerSegmentWriterModule::recordSegmentForDevice(
        std::shared_ptr<Segment> segment,
        std::shared_ptr<ActionRecorderContext> context) {
        auto deviceSegmentHistory = std::make_shared<DeviceSegmentHistory>(context->device);
        auto deviceSegmentPair = std::make_shared<DeviceSegmentPair>(
                context->device,
                segment
                );

        deviceSegmentHistory->deviceSegmentAssociations->push_back(deviceSegmentPair);
        deviceSegmentHistoryCassandraService->pushToWriteBatchQueue(deviceSegmentHistory);


        entityToModuleStateStats->addStateModuleForEntity(
                "ADDING_ACTION_TAKER_DEVICE_SEGMENT",
                "PixelActionTakerSegmentWriterModule",
                EntityToModuleStateStats::all,
                EntityToModuleStateStats::important);

        entityToModuleStateStats->addStateModuleForEntity(
                "ADDING_ACTION_TAKER_DEVICE_SEGMENT",
                "PixelActionTakerSegmentWriterModule",
                segment->getUniqueName());
}

PixelActionTakerSegmentWriterModule::~PixelActionTakerSegmentWriterModule() {

}
