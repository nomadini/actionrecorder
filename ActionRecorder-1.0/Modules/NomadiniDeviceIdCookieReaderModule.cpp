#include "NomadiniDeviceIdCookieReaderModule.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "HttpUtil.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "Device.h"
NomadiniDeviceIdCookieReaderModule::NomadiniDeviceIdCookieReaderModule() {

}

NomadiniDeviceIdCookieReaderModule::~NomadiniDeviceIdCookieReaderModule() {

}

std::string NomadiniDeviceIdCookieReaderModule::getName() {
        return "NomadiniDeviceIdCookieReaderModule";
}

void NomadiniDeviceIdCookieReaderModule::process(std::shared_ptr<ActionRecorderContext> context) {

        std::string nomadiniDeviceIdCookieValue = readOrCreatedNomadiniDeviceIdCookie(
                entityToModuleStateStats,
                context);
        //TODO : add the time of insertion here, so we can delete it later
        context->device = std::make_shared<Device>(nomadiniDeviceIdCookieValue, "");

        setCookie(context);

}

void NomadiniDeviceIdCookieReaderModule::setCookie(std::shared_ptr<ActionRecorderContext> context) {
        Poco::Net::NameValueCollection cookieMapOutgoing;
        cookieMapOutgoing.add("nomadiniDeviceId", context->device->getDeviceId());

        Poco::Net::HTTPCookie cookieOut(cookieMapOutgoing);
        context->httpResponsePtr->addCookie(cookieOut);
        //this is just to assert cookie has been written
        HttpUtil::getCookieFromResponse(*context->httpResponsePtr, "nomadiniDeviceId");
}

std::string NomadiniDeviceIdCookieReaderModule::readOrCreatedNomadiniDeviceIdCookie(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<ActionRecorderContext> context) {
        std::string nomadiniDeviceIdCookieValue;
        auto cookiePairPtr = context->cookieMap.find(NomadiniDeviceIdCookieReaderModule::nomadiniDeviceId);
        if (cookiePairPtr != context->cookieMap.end()) {
                //browser is known to us....
                nomadiniDeviceIdCookieValue = cookiePairPtr->second;
                entityToModuleStateStats->addStateModuleForEntity(
                        "known_browser_seen",
                        "NomadiniDeviceIdCookieReaderModule",
                        "ALL");

        } else {
                //browser is unknown to us
                //we write the mapping to cassandra and to the nomadiniDeviceId cookie
                entityToModuleStateStats->addStateModuleForEntity(
                        "new_browser_seen",
                        "NomadiniDeviceIdCookieReaderModule",
                        "ALL");
                nomadiniDeviceIdCookieValue = Device::createStandardDeviceId();

        }
        return nomadiniDeviceIdCookieValue;
}
