#ifndef ApplicationContext_H
#define ApplicationContext_H


#include "AtomicBoolean.h"
#include <memory>
#include <string>
#include <vector>
#include <fstream>



class ApplicationContext {

private:

public:

static std::shared_ptr<std::ofstream> getBidEventsAppender();
static std::shared_ptr<gicapods::AtomicBoolean> getThreadsInterruptedFlag();
static std::string getTransactionIdConstant();


ApplicationContext();

void info(const std::string& str);

};



#endif
