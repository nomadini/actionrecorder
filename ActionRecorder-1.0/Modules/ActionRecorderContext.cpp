
#include "ActionRecorderContext.h"
#include "JsonUtil.h"
#include "Device.h"
#include "Offer.h"
#include "Pixel.h"
#include "EventLog.h"

ActionRecorderContext::ActionRecorderContext() {
								offerId = -1;
								deviceLat = 0.0;
								deviceLon = 0.0;
								deviceGeoIsAvailable = false;
								pixel = nullptr;
								mgrs1km = 0.0;
								mgrs100m = 0.0;
								mgrs10m = 0.0;
								partnerId = 0;
								strictEventLogChecking = false;//for now it's set to false

}

std::string ActionRecorderContext::toString() {
								return this->toJson();
}


std::string ActionRecorderContext::toJson() {

								return "";
}

ActionRecorderContext::~ActionRecorderContext() {

}
