#ifndef NomadiniDeviceIdCookieReaderModule_H
#define NomadiniDeviceIdCookieReaderModule_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "AtomicLong.h"
#include "ActionRecorderModule.h"
class GicapodsIdToExchangeIdsMapCassandraService;

class NomadiniDeviceIdCookieReaderModule;


class NomadiniDeviceIdCookieReaderModule : public ActionRecorderModule {

public:
static constexpr const char* nomadiniDeviceId = "nomadiniDeviceId";

NomadiniDeviceIdCookieReaderModule();

virtual ~NomadiniDeviceIdCookieReaderModule();

static std::string readOrCreatedNomadiniDeviceIdCookie(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<ActionRecorderContext> context);

std::string getName();
void setCookie(std::shared_ptr<ActionRecorderContext> context);
virtual void process(std::shared_ptr<ActionRecorderContext> context);

};

#endif
