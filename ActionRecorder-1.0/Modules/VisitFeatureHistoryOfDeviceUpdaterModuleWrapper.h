#ifndef VisitFeatureHistoryOfDeviceUpdaterModuleWrapper_H
#define VisitFeatureHistoryOfDeviceUpdaterModuleWrapper_H


#include "ActionRecorderModule.h"
#include "ActionRecorderContext.h"
#include <memory>
#include <string>
#include "ActionRecorderContext.h"
#include "DeviceHistoryUpdaterModule.h"
//NOT USED IN ActionRecorder but we keep it for future use
class DeviceFeatureHistory;
class Feature;

class VisitFeatureHistoryOfDeviceUpdaterModuleWrapper : public ActionRecorderModule {

private:

public:
DeviceHistoryUpdaterModule* visitFeatureDeviceHistoryUpdaterModule;
VisitFeatureHistoryOfDeviceUpdaterModuleWrapper();

std::string getName();

virtual void process(std::shared_ptr<ActionRecorderContext> context);
virtual ~VisitFeatureHistoryOfDeviceUpdaterModuleWrapper();
};



#endif
