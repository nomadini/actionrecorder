#ifndef ActionRecorderRequestHandlerFactory_H
#define ActionRecorderRequestHandlerFactory_H

#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "AtomicBoolean.h"

class OfferCacheService;
class EntityToModuleStateStats;
class CommonRequestHandlerFactory;
#include <tbb/concurrent_hash_map.h>
#include <memory>
#include <unordered_map>
class OfferPixelMapCacheService;
class PixelMatchingPipeline;
class Pixel;
class PixelActionRecorderRequestHandler;
class ActionRecorderHealthService;
class ConversionRecorderPipeline;
class PixelActionRecorderPipeline;

class ActionRecorderRequestHandlerFactory;


class ActionRecorderRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory {

public:
EntityToModuleStateStats* entityToModuleStateStats;
PixelActionRecorderPipeline* pixelActionRecorderPipeline;
ConversionRecorderPipeline* conversionRecorderPipeline;
PixelMatchingPipeline* pixelMatchingPipeline;
OfferCacheService* offerCacheService;
OfferPixelMapCacheService* offerPixelMapCacheService;
ActionRecorderHealthService* actionRecorderHealthService;
CommonRequestHandlerFactory* commonRequestHandlerFactory;
std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<Pixel> > > keyToPixelMap;
std::shared_ptr<gicapods::AtomicBoolean> readyToProcessRequests;
ActionRecorderRequestHandlerFactory(EntityToModuleStateStats* entityToModuleStateStats);
Poco::Net::HTTPRequestHandler* createPixelMatchingHandler(
        std::string exchangeName,
        std::unordered_map<std::string, std::string> queryParams,
        std::unordered_map<std::string, std::string> cookieMap,
        const Poco::Net::HTTPServerRequest& request);
Poco::Net::HTTPRequestHandler* createRequestHandler(const Poco::Net::HTTPServerRequest& request);
};

#endif
