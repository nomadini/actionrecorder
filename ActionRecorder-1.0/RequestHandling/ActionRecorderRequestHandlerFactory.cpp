

#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "ActionRecorderRequestHandlerFactory.h"
#include "PixelActionRecorderRequestHandler.h"
#include "ActionRecorderHealthService.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "UnknownRequestHandler.h"
#include "DoNothingRequestHandler.h"
#include "PixelMatchingRequestHandler.h"
#include "HttpUtil.h"
#include "PixelActionRecorderRequestHandler.h"
#include "OfferCacheService.h"
#include "OfferPixelMapCacheService.h"
#include "CommonRequestHandlerFactory.h"
#include "ConversionRecorderPipeline.h"
#include "PixelMatchingPipeline.h"
#include "PixelActionRecorderPipeline.h"
#include "ConversionRequestHandler.h"

ActionRecorderRequestHandlerFactory::ActionRecorderRequestHandlerFactory(EntityToModuleStateStats* entityToModuleStateStats) {
        this->entityToModuleStateStats = entityToModuleStateStats;
}

Poco::Net::HTTPRequestHandler* ActionRecorderRequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest& request) {

        MLOG(10)<< "ActionRecorderRequestHandlerFactory : request uri to process :"<< request.getURI();
        if (readyToProcessRequests->getValue() == false) {
                entityToModuleStateStats->addStateModuleForEntity("NOT_READY_TO_TAKE_REQUESTS",
                                                                  "ActionRecorderRequestHandlerFactory",
                                                                  EntityToModuleStateStats::all);
                return new DoNothingRequestHandler(entityToModuleStateStats);
        }

        std::string exchangeName = "unknown";

        actionRecorderHealthService->determineHealth();

        std::unordered_map<std::string, std::string> queryParams = HttpUtil::getMapOfQueryParams(request);
        std::unordered_map<std::string, std::string> cookieMap = HttpUtil::getCookiesFromRequest(request);
        for(auto&& cookiePair : cookieMap) {
                LOG_EVERY_N(INFO, 10000) << google::COUNTER<< "th cookie name : "<<cookiePair.first
                                         << " , cookie value : " <<
                        cookiePair.second;
        }
        if (StringUtil::containsCaseInSensitive(request.getURI(), "/rubiconPixel")) {
                entityToModuleStateStats->addStateModuleForEntity("RubiconPixel", "ActionRecorderRequestHandlerFactory", "ALL");

                exchangeName = "rubicon";

                return createPixelMatchingHandler(exchangeName, queryParams, cookieMap, request);
        } else if (StringUtil::containsCaseInSensitive(request.getURI(), "/googlepixelmatching")) {
                entityToModuleStateStats->addStateModuleForEntity("GooglePixel", "ActionRecorderRequestHandlerFactory", "ALL");
                exchangeName = "google";
                return createPixelMatchingHandler(exchangeName, queryParams, cookieMap, request);
        } else if (StringUtil::contains(request.getURI(), "/pixelaction")) {
                entityToModuleStateStats->addStateModuleForEntity("pixelaction", "ActionRecorderRequestHandlerFactory", "ALL");

                auto pixelActionRecorderRequestHandler = new PixelActionRecorderRequestHandler();
                std::shared_ptr<ActionRecorderContext> context = std::make_shared<ActionRecorderContext>();
                context->exchangeName = exchangeName;
                context->queryParams = queryParams;
                context->cookieMap = cookieMap;
                context->httpRequestPtr = &request;

                pixelActionRecorderRequestHandler->context = context;
                pixelActionRecorderRequestHandler->pixelActionRecorderPipeline = pixelActionRecorderPipeline;
                pixelActionRecorderRequestHandler->entityToModuleStateStats = entityToModuleStateStats;
                pixelActionRecorderRequestHandler->keyToPixelMap = keyToPixelMap;
                pixelActionRecorderRequestHandler->offerPixelMapCacheService = offerPixelMapCacheService;
                pixelActionRecorderRequestHandler->offerCacheService = offerCacheService;

                return pixelActionRecorderRequestHandler;
        } else if (StringUtil::contains(request.getURI(), "/conv-pix-action")) {
                entityToModuleStateStats->addStateModuleForEntity("ConversionPixxelAction",
                                                                  "ActionRecorderRequestHandlerFactory",
                                                                  "ALL");

                auto conversionRequestHandler = new ConversionRequestHandler();
                std::shared_ptr<ActionRecorderContext> context = std::make_shared<ActionRecorderContext>();
                context->exchangeName = exchangeName;
                context->queryParams = queryParams;
                context->cookieMap = cookieMap;
                context->httpRequestPtr = &request;

                conversionRequestHandler->context = context;
                conversionRequestHandler->conversionRecorderPipeline = conversionRecorderPipeline;
                conversionRequestHandler->entityToModuleStateStats = entityToModuleStateStats;
                conversionRequestHandler->keyToPixelMap = keyToPixelMap;
                return conversionRequestHandler;
        }

        auto commonRequestHandler = commonRequestHandlerFactory->createRequestHandler(request);
        if (commonRequestHandler != nullptr) {
                return commonRequestHandler;
        }

        entityToModuleStateStats->addStateModuleForEntity("UNKNOWN_REQUEST", "ActionRecorderRequestHandlerFactory", request.getURI());
        return new UnknownRequestHandler(entityToModuleStateStats);
}

Poco::Net::HTTPRequestHandler*  ActionRecorderRequestHandlerFactory::createPixelMatchingHandler(
        std::string exchangeName,
        std::unordered_map<std::string, std::string> queryParams,
        std::unordered_map<std::string, std::string> cookieMap,
        const Poco::Net::HTTPServerRequest& request) {
        auto pixelMatchingRequestHandler = new PixelMatchingRequestHandler();
        std::shared_ptr<ActionRecorderContext> context = std::make_shared<ActionRecorderContext>();
        context->exchangeName = exchangeName;
        context->queryParams = queryParams;
        context->cookieMap = cookieMap;
        context->httpRequestPtr = &request;

        assertAndThrow(!context->exchangeName.empty());
        pixelMatchingRequestHandler->context = context;
        pixelMatchingRequestHandler->pixelMatchingPipeline = pixelMatchingPipeline;
        pixelMatchingRequestHandler->entityToModuleStateStats = entityToModuleStateStats;
        return pixelMatchingRequestHandler;
}
