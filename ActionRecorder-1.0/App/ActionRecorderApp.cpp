//
// Created by Mahmoud Taabodi on 7/11/16.
//


#include "GUtil.h"
#include "CassandraDriverInterface.h"
#include "ConfigService.h"
#include "ActionRecorder.h"
#include "TempUtil.h"
#include "LogLevelManager.h"
#include <thread>
#include "GUtil.h"
#include "StringUtil.h"
#include "ActionEventLogCreatorModule.h"
#include "SignalHandler.h"
#include <string>
#include <memory>
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "StringUtil.h"
#include "GeoSegment.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "OfferPixelMapCacheService.h"
#include "MySqlAdvertiserModelMappingService.h"
#include "DeviceSegmentHistoryCassandraService.h"
#include "SegmentToDeviceCassandraService.h"
#include "MySqlModelService.h"
#include <boost/foreach.hpp>
#include "MySqlTargetGroupService.h"
#include "MySqlCampaignService.h"
#include "MySqlSegmentService.h"
#include "CollectionUtil.h"
#include "DateTimeUtil.h"
#include <thread>
#include "CassandraDriverInterface.h"
#include "EntityToModuleStateStats.h"
#include "CassandraDriverInterface.h"
#include "HttpUtilService.h"
#include "ActionRecorder.h"
#include "ConcurrentHashMap.h"
#include "ModelCacheService.h"
#include "EntityToModuleStateStats.h"
#include "FeatureDeviceHistoryUpdaterModule.h"
#include "DeviceHistoryUpdaterModule.h"
#include "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper.h"
#include "PixelDeviceHistoryCassandraService.h"
#include "PixelDeviceHistoryUpdaterModule.h"
#include "GicapodsIdToPartnerIdMapperModule.h"
#include "ActionRecorderHealthService.h"
#include "PixelMatchingPipeline.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "MySqlMetricService.h"

#include "AsyncThreadPoolService.h"
#include "GicapodsIdToExchangeIdsMapCassandraService.h"

#include "PixelActionTakerSegmentWriterModule.h"
#include "DeviceFeatureHistory.h"
#include "Feature.h"
#include "ActionRecorderAsyncJobService.h"
#include "LoadPercentageBasedSampler.h"
#include "VisitFeatureHistoryOfDeviceUpdaterModuleWrapper.h"
#include "BeanFactory.h"
#include "ConversionDecider.h"
#include "ConversionRecorderPipeline.h"
#include "ServiceFactory.h"
#include "PixelMatchingPipeline.h"
#include "PixelActionRecorderPipeline.h"
#include "NomadiniDeviceIdCookieReaderModule.h"
int main(int argc, char* argv[]) {
        std::string appName = "ActionRecorder";
        TempUtil::configureLogging ("ActionRecorder", argv);
        std::string propertyFileName = "actionrecorder.properties";
        auto beanFactory = std::make_unique<BeanFactory>("SNAPSHOT");

        beanFactory->propertyFileName = propertyFileName;
        beanFactory->commonPropertyFileName = "common.properties";
        beanFactory->appName = appName;
        beanFactory->initializeModules();
        auto serviceFactory = std::make_unique<ServiceFactory>(beanFactory.get());
        serviceFactory->initializeModules();

        auto readyToProcessRequests = std::make_shared<gicapods::AtomicBoolean>(false);

        LOG(INFO) << "google log starting the actionRecorder : argc "<< argc << " argv : " <<  *argv;
        auto keyToPixelMap = std::make_shared<tbb::concurrent_hash_map<std::string, std::shared_ptr<Pixel> > > ();



        auto actionRecorder = std::make_shared<ActionRecorder>
                                      (beanFactory->entityToModuleStateStats.get());

        auto actionRecorderHealthService = std::make_shared<ActionRecorderHealthService>();
        actionRecorderHealthService->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();



        auto actionRecorderRequestHandlerFactory = std::make_shared<ActionRecorderRequestHandlerFactory>
                                                           (beanFactory->entityToModuleStateStats.get());;
        actionRecorderRequestHandlerFactory->actionRecorderHealthService = actionRecorderHealthService.get();
        actionRecorderRequestHandlerFactory->keyToPixelMap = keyToPixelMap;
        actionRecorderRequestHandlerFactory->offerPixelMapCacheService = beanFactory->offerPixelMapCacheService.get();
        actionRecorderRequestHandlerFactory->offerCacheService = beanFactory->offerCacheService.get();


        auto actionRecorderAsyncJobService =
                std::make_shared<ActionRecorderAsyncJobService>(
                        beanFactory->entityToModuleStateStats.get());
        actionRecorderAsyncJobService->mySqlPixelService = beanFactory->mySqlPixelService.get();
        actionRecorderAsyncJobService->keyToPixelMap = keyToPixelMap;
        actionRecorderAsyncJobService->dataReloadService = serviceFactory->dataReloadService.get();
        actionRecorderAsyncJobService->mySqlSegmentService = beanFactory->mySqlSegmentService.get();
        actionRecorderAsyncJobService->entityToModuleStateStatsPersistenceService =
                beanFactory->entityToModuleStateStatsPersistenceService.get();
        actionRecorderAsyncJobService->readyToProcessRequests = readyToProcessRequests;
        actionRecorderAsyncJobService->adInteractionRecordingModule = beanFactory->adInteractionRecordingModule.get();
        actionRecorderAsyncJobService->startAsyncThreads();



        auto nomadiniDeviceIdCookieReaderModule = std::make_shared<NomadiniDeviceIdCookieReaderModule>();
        nomadiniDeviceIdCookieReaderModule->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();


        auto gicapodsIdToPartnerIdMapperModule = std::make_shared<GicapodsIdToPartnerIdMapperModule>();

        gicapodsIdToPartnerIdMapperModule->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        gicapodsIdToPartnerIdMapperModule->gicapodsIdToExchangeIdsMapCassandraService =
                beanFactory->gicapodsIdToExchangeIdsMapCassandraService.get();

        auto conversionDecider = std::make_shared<ConversionDecider>();
        conversionDecider->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        conversionDecider->adHistoryCacheService = beanFactory->adhistoryCacheService.get();
        conversionDecider->targetGroupOfferCacheService = beanFactory->targetGroupOfferCacheService.get();
        conversionDecider->adInteractionRecordingModule = beanFactory->adInteractionRecordingModule.get();
        conversionDecider->eventLogCassandraService = beanFactory->eventLogCassandraService.get();
        conversionDecider->offerPixelMapCacheService = beanFactory->offerPixelMapCacheService.get();

        auto conversionRecorderPipeline = std::make_shared<ConversionRecorderPipeline>();
        conversionRecorderPipeline->modules.push_back(nomadiniDeviceIdCookieReaderModule.get());
        conversionRecorderPipeline->modules.push_back(conversionDecider.get());


        auto pixelActionTakerSegmentWriterModule = std::make_shared<PixelActionTakerSegmentWriterModule>();;
        pixelActionTakerSegmentWriterModule->deviceSegmentHistoryCassandraService = beanFactory->deviceSegmentHistoryCassandraService.get();
        pixelActionTakerSegmentWriterModule->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        pixelActionTakerSegmentWriterModule->offerSegmentCacheService = beanFactory->offerSegmentCacheService.get();

        auto actionEventLogCreatorModule =
                std::make_shared<ActionEventLogCreatorModule>("", "");
        actionEventLogCreatorModule->eventLogCassandraService = beanFactory->eventLogCassandraService.get();
        actionEventLogCreatorModule->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();


        auto pixelMatchingPipeline = std::make_shared<PixelMatchingPipeline>();
        pixelMatchingPipeline->modules.push_back(nomadiniDeviceIdCookieReaderModule.get());
        pixelMatchingPipeline->modules.push_back(gicapodsIdToPartnerIdMapperModule.get());

        auto visitFeatureDeviceHistoryUpdaterModule = std::make_shared<DeviceHistoryUpdaterModule>(
                beanFactory->deviceFeatureHistoryCassandraService.get()
                );
        visitFeatureDeviceHistoryUpdaterModule->featureGuardService = beanFactory->featureGuardService.get();

        visitFeatureDeviceHistoryUpdaterModule->deviceHistorySizeSampler =
                std::make_unique<LoadPercentageBasedSampler>(1, "deviceHistorySizeSampler");

        visitFeatureDeviceHistoryUpdaterModule->deviceIdService = beanFactory->deviceIdService.get();
        visitFeatureDeviceHistoryUpdaterModule->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();

        auto deviceHistoryOfVisitFeatureUpdaterModuleWrapper = std::make_shared<VisitFeatureHistoryOfDeviceUpdaterModuleWrapper>();
        deviceHistoryOfVisitFeatureUpdaterModuleWrapper->visitFeatureDeviceHistoryUpdaterModule = visitFeatureDeviceHistoryUpdaterModule.get();
        deviceHistoryOfVisitFeatureUpdaterModuleWrapper->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();


        auto featureDeviceHistoryUpdaterModule = std::make_shared<FeatureDeviceHistoryUpdaterModule>(
                beanFactory->featureDeviceHistoryCassandraService.get()
                );

        featureDeviceHistoryUpdaterModule->featureGuardService = beanFactory->featureGuardService.get();

        featureDeviceHistoryUpdaterModule->mapOfInterestingFeatures =
                std::make_shared<gicapods::ConcurrentHashMap<std::string, FeatureDeviceHistory> > (
                        beanFactory->entityToModuleStateStats.get()
                        );
        auto randomFeatureSampler = std::make_unique<LoadPercentageBasedSampler>(1, "randomFeatureSampler");//TODO : make me configDriven
        featureDeviceHistoryUpdaterModule->randomFeatureSampler = std::move(randomFeatureSampler);

        featureDeviceHistoryUpdaterModule->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        auto featureDeviceHistoryUpdaterModuleWrapper = std::make_shared<DeviceHistoryOfVisitFeatureUpdaterModuleWrapper>();
        featureDeviceHistoryUpdaterModuleWrapper->featureDeviceHistoryUpdaterModule = featureDeviceHistoryUpdaterModule.get();
        featureDeviceHistoryUpdaterModuleWrapper->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();

        auto pixelDeviceHistoryUpdaterModule = std::make_shared<PixelDeviceHistoryUpdaterModule>(
                beanFactory->pixelDeviceHistoryCassandraService.get()
                );
        pixelDeviceHistoryUpdaterModule->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();

        auto pixelActionRecorderPipeline = std::make_shared<PixelActionRecorderPipeline>();;
        pixelActionRecorderPipeline->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        pixelActionRecorderPipeline->modules.push_back(nomadiniDeviceIdCookieReaderModule.get());
        pixelActionRecorderPipeline->modules.push_back(pixelDeviceHistoryUpdaterModule.get());
        pixelActionRecorderPipeline->modules.push_back(pixelActionTakerSegmentWriterModule.get());
        pixelActionRecorderPipeline->modules.push_back(actionEventLogCreatorModule.get());

        actionRecorderRequestHandlerFactory->conversionRecorderPipeline = conversionRecorderPipeline.get();
        actionRecorderRequestHandlerFactory->pixelActionRecorderPipeline = pixelActionRecorderPipeline.get();
        actionRecorderRequestHandlerFactory->pixelMatchingPipeline = pixelMatchingPipeline.get();
        actionRecorderRequestHandlerFactory->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        actionRecorderRequestHandlerFactory->readyToProcessRequests = readyToProcessRequests;
        actionRecorderRequestHandlerFactory->commonRequestHandlerFactory = serviceFactory->commonRequestHandlerFactory.get();
        actionRecorder->actionRecorderRequestHandlerFactory = actionRecorderRequestHandlerFactory.get();
        actionRecorder->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        actionRecorder->run(argc, argv);

        TempUtil::deleteLockFile("actionRecorder");
        beanFactory->cassandraDriver->closeSessionAndCluster();
}
