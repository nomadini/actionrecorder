
#include "GUtil.h"

#include "FileUtil.h"
#include "DeviceFeatureHistory.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "NumberUtil.h"
#include "ActionRecorderUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "CollectionUtil.h"
#include <shogun/base/init.h>
#include "SignalHandler.h"


void ActionRecorderUtil::setupActionRecorder() {
        std::set_terminate (gicapods::Util::myterminate);
        SignalHandler::installHandlers();

}
